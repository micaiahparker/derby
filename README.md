# derby
simple dice rolling library

```python

from derby import roll

stats = ['str','int','wis','dex','con','cha']
character = { stat: roll('3d6').value for stat in stats } # roll('4d6h3') if your GM is nice enough

```

## examples

```python
roll('1d20+1')
roll('2d20h1+1') # rolling with advantage
roll('4d6h3+1')  # same as roll('4d6+1h3')

# also supports doing comparisons
roll('3d6=18')
roll('1d20>14')
```

